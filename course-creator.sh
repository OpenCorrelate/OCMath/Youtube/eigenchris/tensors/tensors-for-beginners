#! /bin/sh

for i in $(seq -f "%02g" 1 19)
do
  root=videos/vid
  mkdir -p ${root}${i}/{scratch,exercises}
  touch ${root}${i}/{README.md,exercises/README.md}
done
